#!/usr/bin/env bash

apt-get update > /dev/null && apt-get upgrade > /dev/null
if [ $? -eq 0 ]  
then
	echo "[...] update database [...]"	
	echo "[...] upgrade system [...]"
else
	echo "[...] Vous devez être super-utilisateur [/!\]"
fi
