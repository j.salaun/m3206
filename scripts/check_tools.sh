#!/usr/bin/env bash

for cmd in 'git' 'tmux' 'vim' 'htop'
do
	which $cmd > /dev/null
	if [ $? -eq 0 ]
	then
		echo "[...] $cmd: installé      [...]"
	else
		echo "[/!\] $cmd: pas installé  [/!\] lancer la commande: `apt-get install $cmd`"
	fi
done
