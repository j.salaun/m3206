#!/usr/bin/env bash

echo "[...] Checking internet connection [...]"

ping -c 3 8.8.8.8 > /dev/null

if [ $? -eq 0 ]  
then
	echo "[...] Internet access OK    [...]"
else
	echo "[...] Not connected to Internet [/!\]"
	echo "[/!\ Please check configuration [/!\]"
fi
