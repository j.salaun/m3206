#!/usr/bin/env bash

#Script check_internet
echo "[...] Checking internet connection [...]"

ping -c 3 8.8.8.8 > /dev/null

if [ $? -eq 0 ]  
then
	echo "[...] Internet access OK    [...]"
else
	echo "[...] Not connected to Internet [/!\]"
	echo "[/!\ Please check configuration [/!\]"
fi


#Script check_ssh
#Si ssh est installé
which ssh> /dev/null
vlr1=$?

#Si ssh est est lancé
pidof -o 706 -o 704 sshd> /dev/null
vlr2=$?

if [[ $vlr1 -eq  0 && vlr2 -eq 0 ]] 
then
	echo "[...] ssh: fonctionne	[...]"
elif [[ $vlr1 -eq 0 && vlr2 -eq 1 ]]
then
	echo "[...] ssh: installé 	               [...]"
	echo "[/!\] ssh: le service n'est pas lancé  [/!\]"
	echo "[/!\] ssh: lancement du service        [/!\] lancer la commande: `/etc/init.d/ssh start`"
else
	echo "[/!\] ssh: n\'est pas installé	[/!\]"
fi


#Script check_tools
for cmd in 'git' 'tmux' 'vim' 'htop'
do
	which $cmd > /dev/null
	if [ $? -eq 0 ]
	then
		echo "[...] $cmd: installé      [...]"
	else
		echo "[/!\] $cmd: pas installé  [/!\] lancer la commande: `apt-get install $cmd`"
	fi
done


#Script update_system
apt-get update > /dev/null && apt-get upgrade > /dev/null
if [ $? -eq 0 ]  
then
	echo "[...] update database [...]"	
	echo "[...] upgrade system [...]"
else
	echo "[...] Vous devez être super-utilisateur [/!\]"
fi
